# `agrapi`: Another Package to Query ArcGIS REST API

There are a couple of packages around that help you retrieve
data from an ArcGIS REST API. 
Perhaps the most current and full featured is 
[`arcpullr`](https://github.com/pfrater/arcpullr), 
but there is also [getarc](https://github.com/MatthewJWhittle/getarc),
and although I haven't explored it, 
[ESRI make tools available](https://www.esri.com/en-us/arcgis/products/r-arcgis-bridge/overview).

So this begs the question if another package is needed. 
Reading [Why You Should (or Shouldn’t) Build an API Client](https://ropensci.org/blog/2022/06/16/publicize-api-client-yes-no/), 
I  think this attempt falls into:

> Assess potential usage before spending too much effort on your package. Of course usage might 
depend on your efforts: your package might make a data source more accessible to users who 
would feel less at ease writing httr2 code themselves; and your promotion efforts might make 
a wider audience tap into the API your package is wrapping.

> Also, maybe you might want to still develop an API package as a way to learn general 
package development and maintenance skills and to display these skills of yours to a wider audience.

That is, effort was low, learning experience high. Existing packages didn't
quite do what I wanted, and this implements a smaller subset of tasks.
This might grow in future.


## Installation

```R
install.packages('remotes')
remotes::install_gitlab("rdenham/agrapi")
```

## Usage

As an example, The Queensland Government has a lot of spatial data available at 
https://spatial-gis.information.qld.gov.au/arcgis/rest/services

The below extracts the Queensland Basin data as a polygon layer.

```R
library(dplyr)
library(sf)
req = httr2::request("https://spatial-gis.information.qld.gov.au/arcgis/rest/services/Boundaries/AdminBoundariesFramework/FeatureServer/8/")
basins.sf <- get_layer(req, outFields="*")
basins.sf
```

